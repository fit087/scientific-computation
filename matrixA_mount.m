% este é o centro da matriz, com os pontos distantes do contorno
for i=2:nb-1
    
    for j=2:nb-1
        
        
        %indices para diferença finita
        %
        %        k5
        %
        %    k2  k3  k4
        %
        %        k1
        %
        k1=(i-2)*nb+j;
        k2=(i-1)*nb+j-1;
        k3=(i-1)*nb+j;
        k4=(i-1)*nb+j+1;
        k5=i*nb+j;
        
        
        % coeficientes da diferença finita
        A(k3,k1)=1/(D^2);
        A(k3,k2)=1/(D^2);
        A(k3,k3)=-4/(D^2);
        A(k3,k4)=1/(D^2);
        A(k3,k5)=1/(D^2);
        b(k3)=0;
        
        
        
    end
    
end