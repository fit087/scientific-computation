% SOLUÇÃO ANALÍTICA
Ta=zeros(n,n); % solução analitica

for i=1:n
    for j=1:n
        
        % calculando a posição x(m) e y(m) do nó
        xa=(i-1)*D;
        ya=(j-1)*D;
        
        % conforme video
        Ta(j,i)=Tm*(sinh(pi*ya/L)/sinh(pi*L/L))*sin(pi*xa/L)+Tc;        
        
    end
end


w=0;
for i=2:n-1
    
    for j=2:n-1
        
        w=w+1;
        xs(w,1)=Ta(i,j);
        
    end
end


lab=((1:n)*D)-D; %montando vetor de comprimento para o plot

figure(1)
contourf(lab,lab,Ta)
title('Solucao Analitica')
xlabel('X - comprimento (m)')
ylabel('Y - comprimento (m)')