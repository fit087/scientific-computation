% programas computacionais da Lista 6 e Lista 7 - Comp Cientifica

% Programa 3

% ver item 3.7 em http://www.ewp.rpi.edu/hartford/~wallj2/CHT/Notes/ch06.pdf

% Trapezoidal é a discretização no tempo.

% transferencia de calor em uma chapa 2D (quadrada) regime dinâmico
% (transiente)

format long
clear all
clc
close all

%tol=10^(-3);
tol=10^(-8);


n=10; %numero de nós por eixo  (usar n >= 5)

nn=n^2; %numero de nos

L=1; %comprimento dos lados da placa (m)

% Por conveniência,  D = Dx = Dy (distância entre os nós)
D=L/(n-1); % deltaX = deltaY

%
T0=100; % temperatura inicial
Tc=10; % temperatura em todo o contorno



Tempo=1;
nt=150;  %funcionou bem para nt>30
% acho que tem a ver com a estabilidade condicional de Euler
% Forward

dt=Tempo/(nt-1);



S=ones(n,n)*T0;  %grade já com o valor inicial
b=zeros((n-2)^2,1);


% aplicando condições de contorno em S
for i=1:n
    for j=1:n
        
        % fundo
        if (i==1)
            S(i,j)=Tc;
            
        end
        
        %laterais
        if (j==1 || j==n)
            S(i,j)=Tc;
            
        end
        
        %Topo
        if (i==n)
            S(i,j)=Tc;
            
        end
        
    end
end


S1=S;


% metodo incondicionalmente estável. Vou simplificar.

% alfa=0.09; %usando valores para o Ferro
%            % Cp=0.11    rhoF=7900 kg/m3     k=80 W/mK
%alfaxdr=alfa*dt









for p=1:nt
    
    
    
    % Vamos montar o sistema
    
    
    nb=n-2;
    nnb=(n-2)^2;
    A=zeros(nnb,nnb);
    b=zeros(nnb,1);
    
    
    
    i=1;j=1;  % usa contorno em k2 e k1
    
    %indices para diferença finita
    %
    %        k5
    %
    %    k2  k3  k4
    %
    %        k1
    %
    
    k1=(i-2)*nb+j;
    k2=(i-1)*nb+j-1;
    k3=(i-1)*nb+j;
    k4=(i-1)*nb+j+1;
    k5=i*nb+j;
    
    % coeficientes da diferença finita
    %A(k3,k1)=dt/(2*D^2);
    %A(k3,k2)=dt/(2*D^2);
    A(k3,k3)=(-1-2*(dt/(D^2)));
    A(k3,k4)=dt/(2*D^2);
    A(k3,k5)=dt/(2*D^2);
    b(k3)=(-dt/(D^2))*S(i,j+1)+(-dt/(2*D^2))*S(i+2,j+1)+(-1+2*(dt/(D^2)))*S(i+1,j+1)+(-dt/(2*D^2))*S(i+1,j+2)+(-dt/(D^2))*S(i+1,j);
    
    
    
    
    
    
    
    i=1;j=nb;  % usa contorno em k4 e k1
    
    %indices para diferença finita
    %
    %        k5
    %
    %    k2  k3  k4
    %
    %        k1
    %
    k1=(i-2)*nb+j;
    k2=(i-1)*nb+j-1;
    k3=(i-1)*nb+j;
    k4=(i-1)*nb+j+1;
    k5=i*nb+j;
    
    % coeficientes da diferença finita
    %A(k3,k1)=dt/(2*D^2);
    A(k3,k2)=dt/(2*D^2);
    A(k3,k3)=(-1-2*(dt/(D^2)));
    %A(k3,k4)=dt/(2*D^2);
    A(k3,k5)=dt/(2*D^2);
    b(k3)=(-dt/(2*D^2))*S(i,j+1)+(-dt/(D^2))*S(i+2,j+1)+(-1+2*(dt/(D^2)))*S(i+1,j+1)+(-dt/(2*D^2))*S(i+1,j+2)+(-dt/(D^2))*S(i+1,j);
    
    
    
    
    
    
    
    
    
    
    
    
    i=nb;j=nb;  % usa contorno em k5 e k4
    
    %indices para diferença finita
    %
    %        k5
    %
    %    k2  k3  k4
    %
    %        k1
    %
    k1=(i-2)*nb+j;
    k2=(i-1)*nb+j-1;
    k3=(i-1)*nb+j;
    k4=(i-1)*nb+j+1;
    k5=i*nb+j;
    
    
    % coeficientes da diferença finita
    A(k3,k1)=dt/(2*D^2);
    A(k3,k2)=dt/(2*D^2);
    A(k3,k3)=(-1-2*(dt/(D^2)));
    %A(k3,k4)=dt/(2*D^2);
    %A(k3,k5)=dt/(2*D^2);
    b(k3)=(-dt/(2*D^2))*S(i,j+1)+(-dt/(D^2))*S(i+2,j+1)+(-1+2*(dt/(D^2)))*S(i+1,j+1)+(-dt/(D^2))*S(i+1,j+2)+(-dt/(2*D^2))*S(i+1,j);
    
    
    
    
    
    
    
    
    
    i=nb;j=1;  % usa contorno em k5 e k2
    
    %indices para diferença finita
    %
    %        k5
    %
    %    k2  k3  k4
    %
    %        k1
    %
    k1=(i-2)*nb+j;
    k2=(i-1)*nb+j-1;
    k3=(i-1)*nb+j;
    k4=(i-1)*nb+j+1;
    k5=i*nb+j;
    
    
    % coeficientes da diferença finita
    A(k3,k1)=dt/(2*D^2);
    %A(k3,k2)=dt/(2*D^2);
    A(k3,k3)=(-1-2*(dt/(D^2)));
    A(k3,k4)=dt/(2*D^2);
    %A(k3,k5)=dt/(2*D^2);
    b(k3)=(-dt/(D^2))*S(i,j+1)+(-dt/(2*D^2))*S(i+2,j+1)+(-1+2*(dt/(D^2)))*S(i+1,j+1)+(-dt/(D^2))*S(i+1,j+2)+(-dt/(2*D^2))*S(i+1,j);
    
    
    
    
    
    
    
    i=1; % acima do fundo, longe da lateral
    
    for j=2:nb-1    % vai usar k1 do contorno
        
        
        %indices para diferença finita
        %
        %        k5
        %
        %    k2  k3  k4
        %
        %        k1
        %
        k1=(i-2)*nb+j;
        k2=(i-1)*nb+j-1;
        k3=(i-1)*nb+j;
        k4=(i-1)*nb+j+1;
        k5=i*nb+j;
        
        % coeficientes da diferença finita
        %A(k3,k1)=dt/(2*D^2);
        A(k3,k2)=dt/(2*D^2);
        A(k3,k3)=(-1-2*(dt/(D^2)));
        A(k3,k4)=dt/(2*D^2);
        A(k3,k5)=dt/(2*D^2);
        b(k3)=(-dt/(2*D^2))*S(i,j+1)+(-dt/(2*D^2))*S(i+2,j+1)+(-1+2*(dt/(D^2)))*S(i+1,j+1)+(-dt/(2*D^2))*S(i+1,j+2)+(-dt/(D^2))*S(i+1,j);
        
        
    end
    
    
    
    
    i=nb; % Abaixo do topo, longe da lateral
    
    for j=2:nb-1     % vai usar k5 do contorno
        
        
        %indices para diferença finita
        %
        %        k5
        %
        %    k2  k3  k4
        %
        %        k1
        %
        k1=(i-2)*nb+j;
        k2=(i-1)*nb+j-1;
        k3=(i-1)*nb+j;
        k4=(i-1)*nb+j+1;
        k5=i*nb+j;
        
        
        % coeficientes da diferença finita
        A(k3,k1)=dt/(2*D^2);
        A(k3,k2)=dt/(2*D^2);
        A(k3,k3)=(-1-2*(dt/(D^2)));
        A(k3,k4)=dt/(2*D^2);
        %A(k3,k5)=dt/(2*D^2);
        b(k3)=(-dt/(2*D^2))*S(i,j+1)+(-dt/(2*D^2))*S(i+2,j+1)+(-1+2*(dt/(D^2)))*S(i+1,j+1)+(-dt/(D^2))*S(i+1,j+2)+(-dt/(2*D^2))*S(i+1,j);
        
        
    end
    
    
    
    j=1; % lateral esq, no meio
    
    for i=2:nb-1    % vai usar k2 do contorno
        
        %indices para diferença finita
        %
        %        k5
        %
        %    k2  k3  k4
        %
        %        k1
        %
        k1=(i-2)*nb+j;
        k2=(i-1)*nb+j-1;
        k3=(i-1)*nb+j;
        k4=(i-1)*nb+j+1;
        k5=i*nb+j;
        
        
        % coeficientes da diferença finita
        A(k3,k1)=dt/(2*D^2);
        %A(k3,k2)=dt/(2*D^2);
        A(k3,k3)=(-1-2*(dt/(D^2)));
        A(k3,k4)=dt/(2*D^2);
        A(k3,k5)=dt/(2*D^2);
        b(k3)=(-dt/(D^2))*S(i,j+1)+(-dt/(2*D^2))*S(i+2,j+1)+(-1+2*(dt/(D^2)))*S(i+1,j+1)+(-dt/(2*D^2))*S(i+1,j+2)+(-dt/(2*D^2))*S(i+1,j);
        
        
        
    end
    
    
    
    
    
    
    j=nb; % lateral dir, no meio
    
    for i=2:nb-1   % vai usar k4 do contorno
        
        
        %indices para diferença finita
        %
        %        k5
        %
        %    k2  k3  k4
        %
        %        k1
        %
        k1=(i-2)*nb+j;
        k2=(i-1)*nb+j-1;
        k3=(i-1)*nb+j;
        k4=(i-1)*nb+j+1;
        k5=i*nb+j;
        
        
        % coeficientes da diferença finita
        A(k3,k1)=dt/(2*D^2);
        A(k3,k2)=dt/(2*D^2);
        A(k3,k3)=(-1-2*(dt/(D^2)));
        %A(k3,k4)=dt/(2*D^2);
        A(k3,k5)=dt/(2*D^2);
        b(k3)=(-dt/(2*D^2))*S(i,j+1)+(-dt/(D^2))*S(i+2,j+1)+(-1+2*(dt/(D^2)))*S(i+1,j+1)+(-dt/(2*D^2))*S(i+1,j+2)+(-dt/(2*D^2))*S(i+1,j);
        
        
        
    end
    
    
    
    
    % este é o centro da matriz, com os pontos distantes do contorno
    
    for i=2:nb-1
        
        for j=2:nb-1
            
            
            %indices para diferença finita
            %
            %        k5
            %
            %    k2  k3  k4
            %
            %        k1
            %
            k1=(i-2)*nb+j;
            k2=(i-1)*nb+j-1;
            k3=(i-1)*nb+j;
            k4=(i-1)*nb+j+1;
            k5=i*nb+j;
            
            
            % coeficientes da diferença finita
            A(k3,k1)=dt/(2*D^2);
            A(k3,k2)=dt/(2*D^2);
            A(k3,k3)=(-1-2*(dt/(D^2)));
            A(k3,k4)=dt/(2*D^2);
            A(k3,k5)=dt/(2*D^2);
            b(k3)=(-dt/(2*D^2))*S(i,j+1)+(-dt/(2*D^2))*S(i+2,j+1)+(-1+2*(dt/(D^2)))*S(i+1,j+1)+(-dt/(2*D^2))*S(i+1,j+2)+(-dt/(2*D^2))*S(i+1,j);
            
            
            
        end
        
    end
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    % %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % CG com Precondicionamento diagonal
    % %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    % algoritmo retirado de "painless conjugate gradient
    % item B3 - pg 51
    
    % Precondicionamento (M) baseado no link abaixo. Pg 125 (pg 4 do PDF)
    % http://www.math.iit.edu/~fass/477577_Chapter_16.pdf
    %
    
    
    
    xf=[]; r=[]; d=[]; i=[]; imax=[]; deltanew=[]; delta0=[];
    deltaold=[]; q=[]; alfacg=[]; betacg=[]; M=[]; Mm1=[]; s=[];
    i=1;
    imax=1000;
    
    M=diag(diag(A));  %precondicionamento diagonal (Jacobi) A = L + D + L^T
    Mm1=inv(M);
    
    
    xf(:,1)=zeros(nnb,1); %x0=0
    r(:,1)=b-A*xf(:,1);
    r0=r;
    d=Mm1*r;
    deltanew=r(:,1)'*d(:,1);
    delta0=deltanew;
    
    while (i<imax) %&& (deltanew > tol^2*delta0)
        
        q(:,1)=A*d(:,1);
        alfacg=deltanew/(d(:,1)'*q(:,1));
        xold=xf;
        xf(:,1)=xf(:,1)+alfacg*d(:,1);
        xnew=xf;
        
        if (norm(xold-xnew)<tol)
            
            break
        end
        
        if (mod(i,50)==0)
            r(:,1)=b-A*xf(:,1);
        else
            r(:,1)=r(:,1)-alfacg*q(:,1);
        end
        
        s(:,1)=Mm1*r(:,1);
        deltaold=deltanew;
        deltanew=r(:,1)'*s(:,1);
        betacg=deltanew/deltaold;
        d(:,1)=s(:,1)+betacg*d(:,1);
        i=i+1;
        
        
    end
    
    
    
    % montando a solução e aplicando em S
    B=[];
    for k=1:nnb
        i=ceil(k/nb); %obtendo i do nó
        j=k-(i-1)*nb; %obtendo j do nó
        Bcgp(i,j)=xf(k);
    end
    
    Scgp=S;
    S(2:n-1,2:n-1)=Bcgp;
    
    
    
    np(p)=norm(b-A*xf)/norm(r0);
    
%     
%     if (norm(S-Scgp)<=tol)
%         
%         'conv'
%        break 
%     end
%     
    

     if (np(p)<tol)
         'norma matriz ok'
         break
         
     end
    

    
    
end



tol

icgp=p



lab=((1:n)*D)-D; %montando vetor de comprimento para o plot

figure(4)
surfc(lab,lab,Scgp)
zlim([0 max(T0,Tc)])
title('Solucao CG - Precondicionamento Diagonal')
xlabel('X - comprimento (m)')
ylabel('Y - comprimento (m)')



% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%











% 
% 
% %SOLUÇÃO ATRAVÉS DO SOLVER DO MATLAB (Só para verificação)
% 
% x=A\b;
% 
% 
% for k=1:nnb
%     i=ceil(k/nb); %obtendo i do nó
%     j=k-(i-1)*nb; %obtendo j do nó
%     B(i,j)=x(k);
% end
% 
% S(2:n-1,2:n-1)=B;
% 
% Tmlab=S;


