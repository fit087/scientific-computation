# COC 757 - Computação Científica

Algoritmos desenvolvidos no transcurso da disciplina de Computação Científica do
curso de mestrado/doutorado da COPPE/UFRJ.
Os algoritmos foram desenvolvidos em Matlab e foram testados em GNU Octave

1. Introdução: Computação Científica;
2. Solução de Sistemas Lineares;
3. Métodos de Mínimos Quadrados;
4. Autovalores e Valores Singulares;
5. Equações Não-Lineares;
6. Otimização;
7. Interpolação;
8. Integração e Diferenciação Numéricas;
9. Problemas de Valor Inicial para Equações Diferenciais Ordinárias;
10. Problemas de Valor de Contorno para Equações Diferenciais Ordinárias;
11. Equações Diferenciais Parciais;
12. Transformada Rápida de Fourier;
13. Números Aleatórios e Simulação Estocástica.

## Bibliografia

1. J.J. Dongarra, I. Duff, D.C. Sorensen and H. Van der Vorst, Numerical, Linear Algebra for High Performance Computers, SIAM, Philadelphia, 1998.
2. B. Lucquin, O. Pironneau, Introduction to Scientific Computing, Wiley, 1998.
3. Computational Science Education Project, http://csep1.phy.ornl.gov/csep.html , 1996.
4. M. Heath, Scientific Computing: An Introdutory Survey, McGraw-Hill, 1997.
5. C.F. Van Loan, Introduction to Scientific Computing, 2nd Edition, Prentice-Hall, 2000.