% programas computacionais da Lista 6 e Lista 7 - Comp Cientifica

% Programa 3

% ver item 3.7 em http://www.ewp.rpi.edu/hartford/~wallj2/CHT/Notes/ch06.pdf

% Euler-forward é a discretização no tempo. 

% transferencia de calor em uma chapa 2D (quadrada) regime dinâmico
% (transiente)

format long
clear all
clc
close all

tol=10^(-3);
%tol=10^(-8);


n=10; %numero de nós por eixo  (usar n >= 5)

nn=n^2; %numero de nos

L=1; %comprimento dos lados da placa (m)

% Por conveniência,  D = Dx = Dy (distância entre os nós)
D=L/(n-1); % deltaX = deltaY

%
T0=100; % temperatura inicial 
Tc=10; % temperatura em todo o contorno



Tempo=1; 
nt=150;  %funcionou bem para nt>30  
        % acho que tem a ver com a estabilidade condicional de Euler
        % Forward

dt=Tempo/(nt-1);



S=ones(n,n)*T0;  %grade já com o valor inicial
b=zeros((n-2)^2,1);


% aplicando condições de contorno em S
for i=1:n
    for j=1:n
        
        % fundo
        if (i==1)
            S(i,j)=Tc;
            
        end
        
        %laterais
        if (j==1 || j==n)
            S(i,j)=Tc;
            
        end
        
        %Topo
        if (i==n)            
            S(i,j)=Tc;
            
        end
        
    end
end


S1=S;

alfa=0.09; %usando valores para o Ferro
           % Cp=0.11    rhoF=7900 kg/m3     k=80 W/mK
           
alfaxdr=alfa*dt

for p=1:nt
    
    Ca=(alfa*dt)/(D^2);
    Cb=alfa*(1-4*Ca);
    
    for i=2:n-1
        
        for j=2:n-1
            
            S(i,j)=Ca*S(i-1,j)+Ca*S(i+1,j)+Cb*S(i,j)+Ca*S(i,j-1)+Ca*S(i,j+1);
            
            
            % fazendo um critério para evitar números muito próximos de
            % zero apresentando variações no gráfico
            if (S(i,j)>0 && S(i,j)<=tol)
               S(i,j)=0; 
            end
            
        end
        
    end
    
     
    if (p==1 || p==2 || p==3 || p==5 || p==10) %visualizando uma parcial
        
        figure(p)
        imagesc(S)
        xlabel(strcat('t= ',num2str(p)))
        
        
    end
    
    
    
    
    
    
end



figure(100)

subplot(1,2,1)
imagesc(S1)
xlabel('t inicial')
subplot(1,2,2)
imagesc(S)
xlabel('t final')


