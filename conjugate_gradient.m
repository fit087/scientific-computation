% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% CG
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% algoritmo retirado de "painless conjugate gradient
% item B2 - pg 50


xf=[]; r=[]; d=[]; i=[]; imax=[]; deltanew=[]; delta0=[];
deltaold=[]; q=[]; alfacg=[]; betacg=[];
i=1;
imax=1000;

xf(:,1)=zeros(nnb,1); %x0=0
r(:,1)=b-A*xf(:,1);
d=r;
deltanew=r(:,1)'*r(:,1);
delta0=deltanew;

while (i<imax) && (deltanew > tol^2*delta0)

    q(:,1)=A*d(:,1);
    alfacg=deltanew/(d(:,1)'*q(:,1));
    xf(:,1)=xf(:,1)+alfacg*d(:,1);
    
    if (mod(i,50)==0)
        r(:,1)=b-A*xf(:,1); 
    else
        r(:,1)=r(:,1)-alfacg*q(:,1); 
    end

    deltaold=deltanew;
    deltanew=r(:,1)'*r(:,1);
    betacg=deltanew/deltaold;
    d(:,1)=r(:,1)+betacg*d(:,1);
    i=i+1;    
    

end

xkcg=xf;


tol

icg=i



% montando a solução e aplicando em S
B=[];
for k=1:nnb    
    i=ceil(k/nb); %obtendo i do nó
    j=k-(i-1)*nb; %obtendo j do nó    
    Bcg(i,j)=xkcg(k);    
end

Scg=S;
Scg(2:n-1,2:n-1)=Bcg;

errocg=norm(xs-xkcg)



lab=((1:n)*D)-D; %montando vetor de comprimento para o plot

figure(3)
contourf(lab,lab,Scg)
title('Solucao CG - Sem precondicionamento')
xlabel('X - comprimento (m)')
ylabel('Y - comprimento (m)')



% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%