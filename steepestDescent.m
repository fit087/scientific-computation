%  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  %SD
%  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% algoritmo retirado de "painless conjugate gradient
% item 4 - pg 8 - Eq 10, 11 e 12

xf(:,1)=zeros(nnb,1); %x0=0


for i=2:1000
    r(:,i-1)=b-A*xf(:,i-1);
    alfasd(i-1)=(r(:,i-1)'*r(:,i-1))/(r(:,i-1)'*A*r(:,i-1)); 
    xf(:,i)=xf(:,i-1)+alfasd(i-1)*r(:,i-1);
    
    if (norm(xf(:,i)-xf(:,i-1))<tol)        
        %'norma ok'
        break
    end
    
end

xksd=xf(:,i);

tol

isd=i



% montando a solução e aplicando em S
B=[];
for k=1:nnb    
    i=ceil(k/nb); %obtendo i do nó
    j=k-(i-1)*nb; %obtendo j do nó    
    Bsd(i,j)=xksd(k);    
end

Ssd=S;
Ssd(2:n-1,2:n-1)=Bsd;

errosd=norm(xs-xksd)



lab=((1:n)*D)-D; %montando vetor de comprimento para o plot

figure(2)
contourf(lab,lab,Ssd)
title('Solucao Steepest Descent')
xlabel('X - comprimento (m)')
ylabel('Y - comprimento (m)')

%  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%