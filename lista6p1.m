% programas computacionais da Lista 6 e Lista 7 - Comp Cientifica

% Programa 1

% transferencia de calor em uma chapa 2D (quadrada)
format long
clear all
clc
close all

%tol=10^(-3);
tol=10^(-8);


n=6; %numero de nós por eixo  (usar n >= 5)

nn=n^2; %numero de nos

L=1; %comprimento dos lados da placa (m)

% Por conveniência,  D = Dx = Dy (distância entre os nós)
D=L/(n-1); % deltaX = deltaY

%igual ao video
Tc=100; % temperatura do contorno (fundo e laterais)
Tm=10;  % parâmetro de variação do contorno (fundo e laterais) Tc e do Topo



S=zeros(n,n);  %grade
b=zeros((n-2)^2,1);


% aplicando condições de contorno em S
contourn_condition





% este é o centro da matriz, com os pontos distantes do contorno

matrixA_mount




% SOLUÇÃO ANALÍTICA

analytical_solution

 
% % AQUI DEVEM ENTRAR AS SOLUÇÕES USANDO SD E CG SOLICITADAS!!
% 
% % A e b já estão prontos!!
% 
% % f(x)=1/2 xT A x - xT b
% 
% % gf=graf(f(x)) = A x - b


%  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  %SD
%  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% algoritmo retirado de "painless conjugate gradient
% item 4 - pg 8 - Eq 10, 11 e 12

steepestDescent
%  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% CG
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% algoritmo retirado de "painless conjugate gradient
% item B2 - pg 50


conjugate_gradient

% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% CG com Precondicionamento diagonal
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
conjugate_gradient_pre_cond
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



% 
% d=[];r=[];alfacg=[];xf=[];betacg=[];
% 
% xf(:,1)=zeros(nnb,1); %x0=0
% d(:,1)=b-A*xf(:,1);
% r(:,1)=b-A*xf(:,1);
% alfacg(1)=(r(:,1)'*r(:,1))/(r(:,1)'*A*r(:,1)); 
% 
% for i=2:1000
% 
%     xf(:,i)=xf(:,i-1)+alfacg(i-1)*d(:,i-1);
%     
%     if (norm(xf(:,i)-xf(:,i-1))<tol)        
%         %'norma ok'
%         break
%     end
%     
%     r(:,i)=r(:,i-1)-alfacg(i-1)*A*d(:,i-1);
%     
%     betacg(i)=(r(:,i)'*r(:,i))/(r(:,i-1)'*r(:,i-1)); 
%     
%     d(:,i)=r(i)+betacg(i)*d(:,i-1);
%     
%     alfacg(i)=(r(:,i)'*r(:,i))/(r(:,i-1)'*A*r(:,i-1)); 
%     
%     
% end
% 
% xkcg=xf(:,i);
% 
% tol
% 
% icg=i
% 
% 
% 
% 




% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%





%
%
%
% % SOLUÇÃO ATRAVÉS DO SOLVER DO MATLAB (Só para verificação)
%
% x=A\b;
%
%
% for k=1:nnb
%     i=ceil(k/nb); %obtendo i do nó
%     j=k-(i-1)*nb; %obtendo j do nó
%     B(i,j)=x(k);
% end
%
% S(2:n-1,2:n-1)=B;
%
% Tmlab=S;





%
%
% % Plotando resultados prévios
%
% q=abs(Tmlab-Ta); %diferença entre os resultados
% qmax=max(max(q)); %valor máximo da diferença (q)
%
%
% lab=((1:n)*D)-D; %montando vetor de comprimento para o plot
%
% figure(1)
% subplot(1,3,1)
% contourf(lab,lab,Tmlab)
% title('Solucao A\\b - MATLAB')
% xlabel('X - comprimento (m)')
% ylabel('Y - comprimento (m)')
%
%
% subplot(1,3,2)
% contourf(lab,lab,Ta)
% title('Solucao Analitica')
% xlabel('X - comprimento (m)')
% ylabel('Y - comprimento (m)')
%
% subplot(1,3,3)
% contourf(lab,lab,q)
% title(strcat('Diferenca Absoluta - max=',num2str(qmax)))
% xlabel('X - comprimento (m)')
% ylabel('Y - comprimento (m)')
%
%
%
%
% B=A-A';
% max(max(B))   % testando se a matriz esta simetrica
%




