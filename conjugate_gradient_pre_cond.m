% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% CG com Precondicionamento diagonal
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% algoritmo retirado de "painless conjugate gradient
% item B3 - pg 51

% Precondicionamento (M) baseado no link abaixo. Pg 125 (pg 4 do PDF)
% http://www.math.iit.edu/~fass/477577_Chapter_16.pdf



xf=[]; r=[]; d=[]; i=[]; imax=[]; deltanew=[]; delta0=[];
deltaold=[]; q=[]; alfacg=[]; betacg=[]; M=[]; Mm1=[]; s=[];
i=1;
imax=1000;

M=diag(diag(A));  %precondicionamento diagonal (Jacobi) A = L + D + L^T
Mm1=inv(M);


xf(:,1)=zeros(nnb,1); %x0=0
r(:,1)=b-A*xf(:,1);
d=Mm1*r;
deltanew=r(:,1)'*d(:,1);
delta0=deltanew;

while (i<imax) %&& (deltanew > tol^2*delta0)

    q(:,1)=A*d(:,1);
    alfacg=deltanew/(d(:,1)'*q(:,1));
    xold=xf;
    xf(:,1)=xf(:,1)+alfacg*d(:,1);
    xnew=xf;
    
    if (norm(xold-xnew)<tol)
        'norma ok'
       break 
    end
    
    if (mod(i,50)==0)
        r(:,1)=b-A*xf(:,1); 
    else
        r(:,1)=r(:,1)-alfacg*q(:,1); 
    end
    
    s(:,1)=Mm1*r(:,1);    
    deltaold=deltanew;
    deltanew=r(:,1)'*s(:,1);
    betacg=deltanew/deltaold;
    d(:,1)=s(:,1)+betacg*d(:,1);
    i=i+1;    
    

end

xkcgp=xf;


tol

icgp=i



% montando a solução e aplicando em S
B=[];
for k=1:nnb    
    i=ceil(k/nb); %obtendo i do nó
    j=k-(i-1)*nb; %obtendo j do nó    
    Bcgp(i,j)=xkcgp(k);    
end

Scgp=S;
Scgp(2:n-1,2:n-1)=Bcgp;

errocgp=norm(xs-xkcgp)



lab=((1:n)*D)-D; %montando vetor de comprimento para o plot

figure(4)
contourf(lab,lab,Scgp)
title('Solucao CG - Precondicionamento Diagonal')
xlabel('X - comprimento (m)')
ylabel('Y - comprimento (m)')



% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%