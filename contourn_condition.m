%Function contourn_conditions

%End

for i=1:n
    for j=1:n
        
        % fundo
        if (i==1)
            S(i,j)=Tc;
            
        end
        
        %laterais
        if (j==1 || j==n)
            S(i,j)=Tc;
            
        end
        
        %Topo
        if (i==n)
            xL=((j-1)*D);
            S(i,j)=Tc+Tm*sin(pi*xL/L);
            
        end
        
    end
end





nb=n-2;
nnb=(n-2)^2;
A=zeros(nnb,nnb);


i=1;j=1;  % usa contorno em k2 e k1

%indices para diferença finita
%
%        k5
%
%    k2  k3  k4
%
%        k1
%

k1=(i-2)*nb+j;
k2=(i-1)*nb+j-1;
k3=(i-1)*nb+j;
k4=(i-1)*nb+j+1;
k5=i*nb+j;

% coeficientes da diferença finita
%A(k3,k1)=1/(D^2);
%A(k3,k2)=1/(D^2);
A(k3,k3)=-4/(D^2);
A(k3,k4)=1/(D^2);
A(k3,k5)=1/(D^2);
b(k3)=b(k3)-S(i+1,j)/(D^2)-S(i,j+1)/(D^2);







i=1;j=nb;  % usa contorno em k4 e k1

%indices para diferença finita
%
%        k5
%
%    k2  k3  k4
%
%        k1
%
k1=(i-2)*nb+j;
k2=(i-1)*nb+j-1;
k3=(i-1)*nb+j;
k4=(i-1)*nb+j+1;
k5=i*nb+j;

% coeficientes da diferença finita
%A(k3,k1)=1/(D^2);
A(k3,k2)=1/(D^2);
A(k3,k3)=-4/(D^2);
%A(k3,k4)=1/(D^2);
A(k3,k5)=1/(D^2);
b(k3)=b(k3)-S(i,j+1)/(D^2)-S(i+1,j+2)/(D^2);












i=nb;j=nb;  % usa contorno em k5 e k4

%indices para diferença finita
%
%        k5
%
%    k2  k3  k4
%
%        k1
%
k1=(i-2)*nb+j;
k2=(i-1)*nb+j-1;
k3=(i-1)*nb+j;
k4=(i-1)*nb+j+1;
k5=i*nb+j;


% coeficientes da diferença finita
A(k3,k1)=1/(D^2);
A(k3,k2)=1/(D^2);
A(k3,k3)=-4/(D^2);
%A(k3,k4)=1/(D^2);
%A(k3,k5)=1/(D^2);
b(k3)=b(k3)-S(i+2,j+1)/(D^2)-S(i+1,j+2)/(D^2);









i=nb;j=1;  % usa contorno em k5 e k2

%indices para diferença finita
%
%        k5
%
%    k2  k3  k4
%
%        k1
%
k1=(i-2)*nb+j;
k2=(i-1)*nb+j-1;
k3=(i-1)*nb+j;
k4=(i-1)*nb+j+1;
k5=i*nb+j;


% coeficientes da diferença finita
A(k3,k1)=1/(D^2);
%A(k3,k2)=1/(D^2);
A(k3,k3)=-4/(D^2);
A(k3,k4)=1/(D^2);
%A(k3,k5)=1/(D^2);
b(k3)=b(k3)-S(i+2,j+1)/(D^2)-S(i+1,j)/(D^2);







i=1; % acima do fundo, longe da lateral

for j=2:nb-1    % vai usar k1 do contorno
    
    
    %indices para diferença finita
    %
    %        k5
    %
    %    k2  k3  k4
    %
    %        k1
    %
    k1=(i-2)*nb+j;
    k2=(i-1)*nb+j-1;
    k3=(i-1)*nb+j;
    k4=(i-1)*nb+j+1;
    k5=i*nb+j;
    
    % coeficientes da diferença finita
    %A(k3,k1)=1/(D^2);
    A(k3,k2)=1/(D^2);
    A(k3,k3)=-4/(D^2);
    A(k3,k4)=1/(D^2);
    A(k3,k5)=1/(D^2);
    b(k3)=b(k3)-S(i,j+1)/(D^2);
    
    
end




i=nb; % Abaixo do topo, longe da lateral

for j=2:nb-1     % vai usar k5 do contorno
    
    
    %indices para diferença finita
    %
    %        k5
    %
    %    k2  k3  k4
    %
    %        k1
    %
    k1=(i-2)*nb+j;
    k2=(i-1)*nb+j-1;
    k3=(i-1)*nb+j;
    k4=(i-1)*nb+j+1;
    k5=i*nb+j;
    
    
    % coeficientes da diferença finita
    A(k3,k1)=1/(D^2);
    A(k3,k2)=1/(D^2);
    A(k3,k3)=-4/(D^2);
    A(k3,k4)=1/(D^2);
    %A(k3,k5)=1/(D^2);
    b(k3)=b(k3)-S(i+2,j+1)/(D^2);
    
    
end



j=1; % lateral esq, no meio

for i=2:nb-1    % vai usar k2 do contorno
    
    %indices para diferença finita
    %
    %        k5
    %
    %    k2  k3  k4
    %
    %        k1
    %
    k1=(i-2)*nb+j;
    k2=(i-1)*nb+j-1;
    k3=(i-1)*nb+j;
    k4=(i-1)*nb+j+1;
    k5=i*nb+j;
    
    
    % coeficientes da diferença finita
    A(k3,k1)=1/(D^2);
    %A(k3,k2)=1/(D^2);
    A(k3,k3)=-4/(D^2);
    A(k3,k4)=1/(D^2);
    A(k3,k5)=1/(D^2);
    b(k3)=b(k3)-S(i+1,j)/(D^2);
    
    
    
end






j=nb; % lateral dir, no meio

for i=2:nb-1   % vai usar k4 do contorno
    
    
    %indices para diferença finita
    %
    %        k5
    %
    %    k2  k3  k4
    %
    %        k1
    %
    k1=(i-2)*nb+j;
    k2=(i-1)*nb+j-1;
    k3=(i-1)*nb+j;
    k4=(i-1)*nb+j+1;
    k5=i*nb+j;
    
    
    % coeficientes da diferença finita
    A(k3,k1)=1/(D^2);
    A(k3,k2)=1/(D^2);
    A(k3,k3)=-4/(D^2);
    %A(k3,k4)=1/(D^2);
    A(k3,k5)=1/(D^2);
    b(k3)=b(k3)-S(i+1,j+2)/(D^2);
    
    
    
end